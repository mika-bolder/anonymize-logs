package main

import (
	"flag"
	"fmt"
	"time"
)

// main parses arguments from command line and performs anonymization
func main() {

	inputPath := flag.String("input_path", "", "File path of the log to anonymize.")
	outputPath := flag.String("output_path", "", "File path to write the anonymized log into.")
	dbPath := flag.String("db_path", "ip_database/IP2LOCATION-LITE-DB1.BIN", "File path to the ip2location database")
	flag.Usage = explainUsage
	flag.Parse()

	checkArguments(*inputPath, *outputPath, *dbPath)

	fmt.Printf("Will anonymize log in: %s", *inputPath)
	start := time.Now()
	writtenRowCount, err := AnonymizeLog(*inputPath, *outputPath, *dbPath)
	if err != nil {
		fmt.Printf("Error: %v", err)
	}
	end := time.Now()
	elapsedSeconds := end.Sub(start).Seconds()

	printOutput(*outputPath, writtenRowCount, elapsedSeconds)
}
