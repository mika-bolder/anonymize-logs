module anonymize

go 1.12

require (
	github.com/ip2location/ip2location-go v8.3.0+incompatible
	github.com/stretchr/testify v1.6.1
)
