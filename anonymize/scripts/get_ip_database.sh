#!/bin/bash -eu

LOCATION_DB_DIR="ip_database"
LOCATION_DB_FILE_PATH="$LOCATION_DB_DIR/IP2LOCATION-LITE-DB1.BIN.ZIP"
LOCATION_DB_MD5_PATH="$LOCATION_DB_DIR/IP2LOCATION-LITE-DB1.BIN.ZIP.md5"

# get ip2location lite database (ip to country)
wget -O $LOCATION_DB_FILE_PATH \
  https://download.ip2location.com/lite/IP2LOCATION-LITE-DB1.BIN.ZIP

# unzip
unzip $LOCATION_DB_FILE_PATH -d $LOCATION_DB_DIR
