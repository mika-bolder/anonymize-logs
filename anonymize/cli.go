package main

import (
	"flag"
	"fmt"
	"os"
)

func checkArguments(inputPath string, outputPath string, dbPath string) {
	if _, err := os.Stat(inputPath); os.IsNotExist(err) {
		fmt.Printf("Can't open log file to anonymize: %s\n", inputPath)
		explainUsage()
		os.Exit(1)
	}
}

func explainUsage() {
	fmt.Fprintf(os.Stderr, "usage: anonymize_logs [log_file_path] [anonymized_log_file_path]\n")
	flag.PrintDefaults()
}

func printOutput(anonymizedLogFilePath string, writtenRowCount int64, elapsedSeconds float64) {
	wroteOutput, err := os.Stat(anonymizedLogFilePath)
	if err != nil {
		os.Exit(1)
	}
	outputSizeMB := wroteOutput.Size() / (1024 * 1024)
	fmt.Printf("\nWrote: %d rows (%v MB) \n", writtenRowCount, outputSizeMB)
	fmt.Printf("To file: %s\n", anonymizedLogFilePath)
	fmt.Printf("Took: %v seconds\n", elapsedSeconds)
}
