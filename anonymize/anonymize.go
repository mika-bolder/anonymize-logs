package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/ip2location/ip2location-go"
)

// Regex for finding ipv4 addresses
var (
	ipRegex, _ = regexp.Compile(`\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b`)
)

// AnonymizeRow takes a single row of log as string.
// Returns it with ip address replaced with country short code (ISO 3166)
func AnonymizeRow(row string, db *ip2location.DB) string {

	// If there's no IP to be found in the row, just return row (no PII)
	foundIP := ipRegex.Find([]byte(row))
	if foundIP == nil {
		return row
	}

	// If there's no matching county (indicated by '-' ), return unknown (could still be potentially PII)
	foundIPString := string(foundIP)
	matchingCountry, err := db.Get_country_short(foundIPString)
	if err != nil || matchingCountry.Country_short == "-" {
		return strings.Replace(row, foundIPString, "UNK", -1)
	}
	matchingCountryCode := matchingCountry.Country_short
	return strings.Replace(row, foundIPString, matchingCountryCode, -1)
}

// AnonymizeLog takes filepath where raw logs are, output path for the anonymized logs and path to the location2ip database.
// Returns number of rows of anonymized logs written
func AnonymizeLog(inputPath string, outputPath string, dbPath string) (int64, error) {

	db, err := ip2location.OpenDB(dbPath)
	if err != nil {
		fmt.Printf("Can't read the ip2location database - please use a valid database binary\n")
		return 0, err
	}

	inputFile, err := os.Open(inputPath)
	if err != nil {
		fmt.Printf("Can't read from the provided input file: %s\n", inputPath)
		return 0, err
	}
	defer inputFile.Close()

	inputReader := bufio.NewScanner(inputFile)
	outputFile, err := os.Create(outputPath)
	if err != nil {
		return 0, err
	}
	defer outputFile.Close()
	outputWriter := bufio.NewWriter(outputFile)
	defer outputWriter.Flush()

	writtenRows := int64(0)
	for inputReader.Scan() {
		row := inputReader.Text()
		anonymizedRow := AnonymizeRow(row, db)
		fmt.Fprintln(outputWriter, anonymizedRow)
		writtenRows = writtenRows + 1
	}
	return writtenRows, nil
}
