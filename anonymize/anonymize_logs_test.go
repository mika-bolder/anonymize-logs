package main

import (
	"testing"

	"github.com/ip2location/ip2location-go"
	"github.com/stretchr/testify/assert"
)

const validDBPath = "ip_database/IP2LOCATION-LITE-DB1.BIN"
const validSmallInputPath = "log_input/small_input.log"
const validSmallOutputPath = "log_output/small_ouput.log"

func getTestDB() *ip2location.DB {
	db, _ := ip2location.OpenDB(validDBPath)
	return db
}

func TestAnonymizeRowValidRow(t *testing.T) {
	description := "Valid row with US ip should produce US as the country code"
	testRow := "98.192.48.31 - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	expected := "US - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	anonymizedRow := AnonymizeRow(testRow, getTestDB())
	assert.Equal(t, expected, anonymizedRow, description)
}

func TestAnonymizeRowMissingIP(t *testing.T) {
	description := "Row without IP should produce the row itself"
	testRow := "98.192 - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	expected := "98.192 - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	anonymizedRow := AnonymizeRow(testRow, getTestDB())
	assert.Equal(t, expected, anonymizedRow, description)
}

func TestAnonymizeRowUnknownCountry(t *testing.T) {
	description := "Row with unknown country should produce UNK (Unknown)"
	testRow := "253.24.201.223 - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	expected := "UNK - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804"
	anonymizedRow := AnonymizeRow(testRow, getTestDB())
	assert.Equal(t, expected, anonymizedRow, description)
}

func TestAnonymizeLogDatabaseLoadFail(t *testing.T) {
	description := "Anonymization with an invalid database binary should fail"
	_, err := AnonymizeLog("", "", "")
	assert.NotEqual(t, err, nil, description)
}

func TestAnonymizeLogInvalidInputPath(t *testing.T) {
	description := "Anonymization with an invalid input path should fail"
	_, err := AnonymizeLog("", validSmallOutputPath, validDBPath)
	assert.NotEqual(t, err, nil, description)
}

func TestAnonymizeLogInvalidOutputPath(t *testing.T) {
	description := "Anonymization with an invalid input path should fail"
	_, err := AnonymizeLog(validSmallInputPath, "", validDBPath)
	assert.NotEqual(t, err, nil, description)
}

func TestLogAnonymizerSmallExample(t *testing.T) {
	description := "Small test file should return 100000 rows"
	expectedOutput := int64(100000)
	defaultDbPath := "ip_database/IP2LOCATION-LITE-DB1.BIN"

	result, err := AnonymizeLog(validSmallInputPath, validSmallOutputPath, defaultDbPath)
	assert.Equal(t, result, expectedOutput, description)
	assert.Equal(t, err, nil, description)
}
