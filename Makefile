#!/usr/bin/make

# Propagate environment variables and set project level variables
.EXPORT_ALL_VARIABLES:

default:
	echo "Please use one of the valid commands."

build-generator:
	(cd generate-random-logs && go build)

generate-small-log:
	(cd generate-random-logs && ./generate-random-logs)

fetch-anonymize-database:
	(cd anonymize && bash -e scripts/get_ip_database.sh)

build-anonymizer:
	(cd anonymize && go build)

run-example:
	(cd anonymize && ./anonymize --input_path=log_input/small_input.log --output_path=log_output/small_output.log --db_path=ip_database/IP2LOCATION-LITE-DB1.BIN)

test:
	(cd anonymize && go test -v)
