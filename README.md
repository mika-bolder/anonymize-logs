# Demo: anonymize web logs

Simple demo on how to anonymize apache web log IPv4 addresses to corresponding ISO 3166 country short code

### Overview

The demo consists of two simple apps written in go:
- generate-random-logs: generates a sequence of test data (log rows)
- anonymize: takes log input file and anonymizes ipv4 addresses

To test out the anonymizer, just follow along the steps below.

### Pre-requisites

The assumption is that these are installed:  

**go (version 1.12 or newer)**  

**make**

**wget**  

### Generate random logs

This simple utility generates log entries of given length with random ip addresses.

To build, run:  

```
make build-generator
```

To generate an example log:
```
  make generate-small-log
```

### Anonymize

Anonymization uses a local ip to country mapping database (ip2location lite).

To fetch the database (gets it from a url), run:
```
make fetch-anonymize-database
```

To build the anonymizer:

```
make build-anonymizer
```

After fetching the database, run the anonymization example:

```
make run-example
```

Or run it in shell (in the anonymize folder):  
```
./anonymize <input_file_path> <output_file_path> ip_database/IP2LOCATION-LITE-DB1.BIN
```

### Tests

To run anonymizer tests:

```
make test
```
