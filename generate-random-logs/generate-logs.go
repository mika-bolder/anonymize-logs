package main

import (
	"bufio"
	"fmt"
	_ "net/http/pprof"
	"os"
	"time"

	"github.com/Pallinder/go-randomdata"
)

func main() {

	n := 100000
	outputFile := "../anonymize/log_input/small_input.log"
	template := "%s - - [28/Jul/2020:04:24:36 +0000] GET /some/path.js?4181443613 HTTP/1.1 200 6804\n"

	f, err := os.Create(outputFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	defer w.Flush()

	start := time.Now()
	for i := 0; i < n; i++ {
		address := randomdata.IpV4Address()
		logRow := fmt.Sprintf(template, address)
		w.WriteString(logRow)
	}
	fmt.Printf("Wrote: %d rows to: %s\n", n, outputFile)
	end := time.Now()
	elapsedSeconds := end.Sub(start).Seconds()

	wroteOutput, err := os.Stat(outputFile)
	if err != nil {
		fmt.Printf("Error writing output: %s", err)
		panic(err)
	}

	outputSizeMB := wroteOutput.Size() / (1024 * 1024)
	fmt.Printf("Wrote: %v MB\n", outputSizeMB)
	fmt.Printf("Took: %v seconds\n", elapsedSeconds)
}
